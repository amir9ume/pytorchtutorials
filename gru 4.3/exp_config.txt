batch_size    20
code_file    /opt/conda/envs/fastai/lib/python3.6/site-packages/ipykernel_launcher.py
data    
debug    False
dp_keep_prob    0.35
emb_size    200
evaluate    False
hidden_size    1500
initial_lr    0.0001
model    GRU
num_epochs    40
num_layers    2
optimizer    ADAM
save_best    False
save_dir    GRU_ADAM_0
seed    1111
seq_len    35
